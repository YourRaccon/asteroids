﻿using UnityEngine;

namespace HoshiSpace
{
    [CreateAssetMenu(fileName = "BulletData", menuName = "BulletData")]
    public class BulletData : ScriptableObject
    {
        [SerializeField] private GameObject _prefab;

        [SerializeField] private float _bulletSpeed;

        [SerializeField] private float _lifeTime;

        #region Getters
        public GameObject Prefab { get => _prefab; }

        public float BulletSpeed { get => _bulletSpeed; }

        public float LifeTime { get => _lifeTime; }
        #endregion
    }
}

