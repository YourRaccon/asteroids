﻿using UnityEngine;

namespace HoshiSpace
{
    [CreateAssetMenu(fileName = "WeaponData", menuName = "WeaponData")]
    public class WeaponData : ScriptableObject
    {
        [SerializeField] private BulletData _bulletData;

        [SerializeField] private float _cooldown;

        #region Getters
        public BulletData BulletData { get => _bulletData; }

        public float Cooldown { get => _cooldown; }
        #endregion
    }
}
