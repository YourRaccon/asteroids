﻿using UnityEngine;

namespace HoshiSpace
{
    [CreateAssetMenu(fileName = "PlayerData", menuName = "PlayerData")]
    public class PlayerData : ScriptableObject
    {
        [SerializeField] private GameObject _prefab;

        [SerializeField] private Vector3 _startPosition;

        [SerializeField] private float _acceleration;
        [SerializeField] private float _maxSpeed;
        [SerializeField] private float _rotationSpeed;

        [SerializeField] private float _friction;

        [SerializeField] private WeaponData _weaponData;

        #region Getters
        public GameObject Prefab { get => _prefab; }

        public Vector3 StartPosition { get => _startPosition; }

        public float Acceleration { get => _acceleration; }
        public float MaxSpeed { get => _maxSpeed; }
        public float RotationSpeed { get => _rotationSpeed; }

        public float Friction { get => _friction; }

        public WeaponData WeaponData { get => _weaponData; }
        #endregion
    }
}
