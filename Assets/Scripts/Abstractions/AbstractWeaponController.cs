﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HoshiSpace
{
    public abstract class AbstractWeaponController : IWeapon
    {
        protected WeaponData _weaponData;

        protected GameObject _shooter;

        private GameManager _gameManager;

        private InitializeController _initializator;

        private float _deltaTime = 0;

        public void Shoot()
        {
            if(!CanShoot())
            {
                return;
            }

            var bullet = _initializator.InitializeBullet(_weaponData.BulletData, this);

            var direction = GetDirection();
            bullet.SetDirection(direction);

            _deltaTime = _weaponData.Cooldown;
        }

        public void UpdateCooldown()
        {
            if(_deltaTime > 0)
            {
                _deltaTime -= Time.deltaTime;
            }
        }

        protected virtual bool CanShoot()
        {
            if (_deltaTime <= 0)
            {
                return true;
            }

            return false;
        }

        public abstract Vector3 GetSpawner();

        public abstract Vector3 GetDirection();

        public void SetWeaponData(WeaponData weaponData)
        {
            _weaponData = weaponData;
        }

        public void SetSpawnPoint(GameObject shooter)
        {
            _shooter = shooter;
        }

        public void SetGameManager(GameManager gameManager)
        {
            _gameManager = gameManager;

            if (_gameManager != null && _initializator == null)
            {
                _initializator = new InitializeController(_gameManager);
            }
        }
    }
}
