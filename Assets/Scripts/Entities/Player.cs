﻿using UnityEngine;

namespace HoshiSpace
{
    public sealed class Player : MonoBehaviour
    {
        [SerializeField] private GameObject _bulletSpawnPoint;

        private PlayerData _playerData;

        private PlayerController _playerController;
        private PlayerWeaponController _playerWeaponController;

        private Collider2D _playerCollider;
        private Rigidbody2D _playerRigidbody;

        private GameManager _gameManager;

        public void Start()
        {
            _playerCollider = GetComponent<Collider2D>();
            _playerRigidbody = GetComponent<Rigidbody2D>();
        }

        public void Init(PlayerData playerData, GameManager gameManager)
        {
            _gameManager = gameManager;

            _playerData = playerData;

            _playerController = new PlayerController(this);
            _playerController.SetPlayerData(_playerData);

            _playerWeaponController = new PlayerWeaponController(this);
            _playerWeaponController.SetGameManager(_gameManager);
            _playerWeaponController.SetWeaponData(_playerData.WeaponData);
            _playerWeaponController.SetSpawnPoint(_bulletSpawnPoint);
        }

        public void Update()
        {
            //Движение
            _playerController.Move();

            //Стрельба
            _playerWeaponController.UpdateCooldown();
            _playerWeaponController.Shoot();
        }

        public Collider2D GetPlayerCollider()
        {
            return _playerCollider;
        }

        public Rigidbody2D GetPlayerRigidbody()
        {
            return _playerRigidbody;
        }
    }
}
