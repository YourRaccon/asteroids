﻿using UnityEngine;

namespace HoshiSpace
{
    public sealed class Bullet : MonoBehaviour
    {
        private BulletData _bulletData;

        private BulletController _bulletController;

        private Collider2D _bulletCollider;
        private Rigidbody2D _bulletRigidbody;

        public void Start()
        {
            _bulletCollider = GetComponent<Collider2D>();
            _bulletRigidbody = GetComponent<Rigidbody2D>();
        }

        public void Init(BulletData bulletData)
        {
            _bulletData = bulletData;

            if (_bulletController == null)
            {
                _bulletController = new BulletController(this);
                _bulletController.SetSpeed(bulletData.BulletSpeed);
                _bulletController.SetLifeTime(bulletData.LifeTime);
            }
            else
            {
                _bulletController.Reset();
            }
        }

        public void Update()
        {
            //Движение вперед
            _bulletController.UpdateLifeTime();
            _bulletController.Move();
        }

        public void SetDirection(Vector3 direction)
        {
            _bulletController.SetDirection(direction);
        }

        public Collider2D GetBulletCollider()
        {
            return _bulletCollider;
        }

        public Rigidbody2D GetBulletRigidbody()
        {
            return _bulletRigidbody;
        }
    }
}
