﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HoshiSpace
{
    public class ObjectPool : MonoBehaviour
    {
        [SerializeField] private GameObject _objectPrefab;

        [SerializeField] private GameObject _poolRoot;

        [SerializeField] private int _capacity;

        private GameObject[] _pooledObjects;

        void Start()
        {
            _pooledObjects = new GameObject[_capacity];

            GameObject gameObject;
            for(int i = 0; i < _capacity; i++)
            {
                gameObject = Instantiate(_objectPrefab, _poolRoot.transform);
                gameObject.SetActive(false);
                _pooledObjects[i] = gameObject;
            }
        }

        public GameObject GetPooledObject()
        {
            foreach(var pooledObject in _pooledObjects)
            {
                if(!pooledObject.activeInHierarchy)
                {
                    return pooledObject;
                }
            }

            return null;
        }
    }
}
