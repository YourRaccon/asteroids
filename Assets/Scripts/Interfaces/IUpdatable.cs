﻿using UnityEngine;

namespace HoshiSpace
{
    public interface IUpdatable
    {
        void UpdateTick();
    }
}
