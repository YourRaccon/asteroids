﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HoshiSpace
{
    public interface IWeapon
    {
        void Shoot();

        Vector3 GetSpawner();

        Vector3 GetDirection();
    }
}
