﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HoshiSpace
{
    public interface IMovable
    {
        void Move();
    }
}
