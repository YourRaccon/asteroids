﻿using UnityEngine;

namespace HoshiSpace
{
    public sealed class PlayerController : IMovable
    {
        private Player _player;

        private PlayerData _playerData;

        public PlayerController(Player player)
        {
            _player = player;
        }

        public void Move()
        {
            //Движение вперед
            float vertical = Input.GetAxis("Vertical");

            if (vertical > 0)
            {
                MoveForvard(vertical);
            }

            //Поворот
            float horizontal = Input.GetAxis("Horizontal");
            
            if(horizontal != 0)
            {
                Rotate(-horizontal);
            }

            //Трение
            SimulateFrictionForce();
        }

        public void MoveForvard(float force)
        {
            var direction = _player.gameObject.transform.up * force;

            var playerSpeedOnDirection = Vector3.Project(_player.GetPlayerRigidbody().velocity, direction);

            //Если playerSpeedOnDirection [проекция скорости игрока на вектор направления] и direction [вектор направления] направлены в разные стороны, то считаем проекцию - нуль-вектором
            if (Vector3.Dot(playerSpeedOnDirection, direction) < 0)
            {
                playerSpeedOnDirection = Vector3.zero;
            }

            if (playerSpeedOnDirection.sqrMagnitude >= _playerData.MaxSpeed * _playerData.MaxSpeed)
            {
                return;
            }

            _player.GetPlayerRigidbody().AddForce(direction * _playerData.Acceleration);
        }

        public void Rotate(float force)
        {
            _player.gameObject.transform.Rotate(Vector3.forward * force * _playerData.RotationSpeed * Time.deltaTime);
        }

        public void SimulateFrictionForce()
        {
            _player.GetPlayerRigidbody().AddForce(-1 * _player.GetPlayerRigidbody().velocity * _playerData.Friction);
        }

        public void SetPlayerData(PlayerData playerData)
        {
            _playerData = playerData;
        }
    }
}
