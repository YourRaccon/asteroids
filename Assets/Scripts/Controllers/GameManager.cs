﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HoshiSpace
{
    public sealed class GameManager : MonoBehaviour
    {
        [SerializeField] private GameObject _spawnedObjectsRoot;
        [SerializeField] private PlayerData _playerData;

        [SerializeField] private ObjectPool _bulletPool;

        private List<IUpdatable> _updatables = new List<IUpdatable>();

        private Camera _mainCamera;

        private Player _player;

        private InitializeController _initializator;

        private bool _gameOnPause = false;

        private void Awake()
        {
            _initializator = new InitializeController(this);
            _gameOnPause = false;

            InitializeLevel();
        }

        public void Start()
        {
            _mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        }

        private void InitializeLevel()
        {
            InitializePlayer();
        }

        private void InitializePlayer()
        {
            _player = _initializator.InitializePlayer(_playerData);
        }

        private void Update()
        {
            for (int i = 0; i < _updatables.Count; i++)
            {
                _updatables[i].UpdateTick();
            }
        }

        public void AddUpdatable(IUpdatable updatable)
        {
            _updatables.Add(updatable);
        }

        public void RemoveUpdatable(IUpdatable updatable)
        {
            _updatables.Remove(updatable);
        }

        public ObjectPool GetBulletPool()
        {
            return _bulletPool;
        }

        public void PauseGame()
        {
            _gameOnPause = true;
        }

        public void ContinueGame()
        {
            _gameOnPause = false;
        }

        public void RestartLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public bool IsGameOnPause()
        {
            return _gameOnPause;
        }

        public GameObject GetSpawnedObjectRoot()
        {
            return _spawnedObjectsRoot;
        }

        public Camera GetMainCamera()
        {
            return _mainCamera;
        }
    }
}
