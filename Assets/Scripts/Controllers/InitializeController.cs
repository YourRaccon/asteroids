﻿using UnityEngine;

namespace HoshiSpace
{
    public sealed class InitializeController
    {
        private GameManager _gameManager;

        private ObjectPool _bulletPool;

        public InitializeController(GameManager gameManager)
        {
            _gameManager = gameManager;
            _bulletPool = _gameManager.GetBulletPool();
        }

        public Player InitializePlayer(PlayerData playerData)
        {
            var spawnedObject = Object.Instantiate(playerData.Prefab, playerData.StartPosition, Quaternion.identity);

            var player = spawnedObject.GetComponent<Player>();

            player.Init(playerData, _gameManager);

            return player;
        }

        public Bullet InitializeBullet(BulletData bulletData, IWeapon weapon)
        {
            var spawnedObject = _bulletPool.GetPooledObject();

            if(spawnedObject == null)
            {
                throw new System.Exception("Bullet pool is empty!!!");
            }

            var bullet = spawnedObject.GetComponent<Bullet>();

            bullet.transform.position = weapon.GetSpawner();

            bullet.Init(bulletData);

            bullet.gameObject.SetActive(true);

            return bullet;
        }
    }
}
