﻿using UnityEngine;

namespace HoshiSpace
{
    public sealed class BulletController : IMovable
    {
        private Bullet _bullet;

        private float _speed;


        private Vector2 _direction;

        private float _lifeTime = 0;
        private float _lifeTimeLeft = 0;

        public BulletController(Bullet bullet)
        {
            _bullet = bullet;
        }

        public void Reset()
        {
            _lifeTimeLeft = 0;
        }

        public void Move()
        {
            if(_direction == null)
            {
                return;
            }

            _bullet.GetBulletRigidbody().MovePosition(_bullet.GetBulletRigidbody().position + _direction * _speed * Time.deltaTime);
        }

        public void UpdateLifeTime()
        {
            if(_lifeTime <= 0)
            {
                return;
            }

            if(_lifeTimeLeft < _lifeTime)
            {
                _lifeTimeLeft += Time.deltaTime;
            }
            else
            {
                _bullet.gameObject.SetActive(false);
            }
        }

        public void SetDirection(Vector2 direction)
        {
            _direction = direction.normalized;

            var angle = Mathf.Atan(_direction.y / _direction.x) * Mathf.Rad2Deg;

            _bullet.gameObject.transform.rotation = Quaternion.Euler(0, 0, angle);
        }

        public void SetSpeed(float speed)
        {
            _speed = speed;
        }

        public void SetLifeTime(float lifeTime)
        {
            _lifeTime= lifeTime;
        }

        private void Destroy()
        {
            GameObject.Destroy(_bullet.gameObject);
        }
    }
}
