﻿using System;
using UnityEngine;

namespace HoshiSpace
{
    public sealed class PlayerWeaponController : AbstractWeaponController
    {
        private Player _player;

        private GameObject _target;

        public PlayerWeaponController(Player player)
        {
            _player = player;
        }

        protected override bool CanShoot()
        {
            var canShoot = base.CanShoot();
            if(!canShoot)
            {
                return canShoot;
            }

            if(Input.GetKey(KeyCode.Space))
            {
                return true;
            }

            return false;
        }

        public override Vector3 GetSpawner()
        {
            return _shooter.transform.position;
        }

        public override Vector3 GetDirection()
        {
            return _shooter.transform.up;
        }
    }
}
